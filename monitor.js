'use strict'

const [, , script] = process.argv

const startTime = (new Date()).getTime()

const { spawn, spawnSync } = require('child_process')
const readline = require('readline')
const numeral = require('numeral')

let max = 0

function displayRss({ pid }) {
  const { output } = spawnSync('ps', ['-o', 'rss', pid])
  const string = output
    .map((a) => (a || '').toString())
    .join('')
    .replace(/[^0-9]/g, '')
  const rss = Number(string) * 1000
  max = Math.max(max, rss)
  readline.cursorTo(process.stdout, 0, 0)
  readline.clearScreenDown(process.stdout, 0)
  // readline.clearLine(process.stdout, 0)

  process.stdout.write('CURRENT  ' + numeral(rss).format('0.000b'))
  process.stdout.write('\n')
  process.stdout.write('MAX      ' + numeral(max).format('0.000b'))
}

// const child = spawn('node', [script])
const child = spawn('node', ['--expose-gc', script])

child.on('exit', () => {
  const endTime = (new Date()).getTime()
  const elapsed = (endTime - startTime) / 1000
  console.log()
  console.log(numeral(elapsed).format('00:00:00'))
  console.log('exit')
  process.exit()
})

child.on('close', () => {
  console.log('close')
  process.exit()
})

child.on('disconnect', () => {
  console.log('disconnect')
  process.exit()
})

process.on('SIGINT', function () {
  const endTime = (new Date()).getTime()
  const elapsed = (endTime - startTime) / 1000
  console.log()
  console.log(numeral(elapsed).format('00:00:00'))
  console.log('exit')
  child.kill()
  process.exit()
})

setInterval(() => displayRss(child), 250)
