'use strict'

const test = require('tap').test

const getRelativeUrl = require('../../lib/helpers/relative-url')

test('', (t) => {

  t.plan(7)

  t.test('classic relative case with mandatory "./" at the beginning', async (t) => {
    const baseUrl = '/the-component/the-module/the-topic'
    const url = '/the-component/the-module/the-topic/the-file.html'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, './the-file.html')
  })

  t.test('fragments should work', async (t) => {
    const baseUrl = '/the-component/the-module/the-topic'
    const url = '/the-component/the-module/the-topic/the-file.html#the-fragment'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, './the-file.html#the-fragment')
  })

  t.test('child page', async (t) => {
    const baseUrl = '/the-component'
    const url = '/the-component/the-module/the-topic/the-file.html'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, './the-module/the-topic/the-file.html')
  })

  t.test('parent page', async (t) => {
    const baseUrl = '/the-component/the-module/the-topic/the-subtopic'
    const url = '/the-component/the-module/the-file.html'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, '../../the-file.html')
  })

  t.test('leading slash should be kept', async (t) => {
    const baseUrl = '/the-component/the-module/the-topic'
    const url = '/the-component/the-module/'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, '../')
  })

  t.test('leading slash should be kept (even for simple cases)', async (t) => {
    const baseUrl = '/the-component/the-module'
    const url = '/the-component/the-module/'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, './')
  })

  t.test('no leading slash should be ignored', async (t) => {
    const baseUrl = '/the-component/the-module'
    const url = '/the-component/the-module/the-file'
    const relativeUrl = getRelativeUrl(baseUrl, url)
    t.equal(relativeUrl, './the-file')
  })
})
