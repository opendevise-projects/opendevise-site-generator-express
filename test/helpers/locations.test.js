'use strict'

const test = require('tap').test

const {
  getSourceFromPath,
  getThemeSourceFromPath,
  getNavigationSourceFromPath,
  getAspectNavigationSourceFromPath,
  getSourceFromXref,
  expandSource,
  extractAspectFromXref,
  getOutput,
} = require('../../lib/helpers/locations')

test('find content/assets/samples source using component, version and path', (t) => {

  t.plan(5)

  t.test('full content example', async (t) => {
    const src = getSourceFromPath('the-component', 'The Component', 'v1.2.3', 'modules/the-module/content/the-topic/the-file.adoc')
    t.equal(src.component, 'the-component')
    t.equal(src.componentTitle, 'The Component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.equal(src.relativeToModule, '../..')
    t.equal(src.area, 'content')
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('content example with subtopic', async (t) => {
    const src = getSourceFromPath('the-component', 'The Component', 'v1.2.3', 'modules/the-module/content/the-topic/the-subtopic/the-file.adoc')
    t.equal(src.component, 'the-component')
    t.equal(src.componentTitle, 'The Component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.equal(src.relativeToModule, '../../..')
    t.equal(src.area, 'content')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('content example without topic', async (t) => {
    const src = getSourceFromPath('the-component', 'The Component', 'v1.2.3', 'modules/the-module/content/the-file.adoc')
    t.equal(src.component, 'the-component')
    t.equal(src.componentTitle, 'The Component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.equal(src.relativeToModule, '..')
    t.equal(src.area, 'content')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('assets example', async (t) => {
    const src = getSourceFromPath('the-component', 'The Component', 'v1.2.3', 'modules/the-module/assets/images/the-file.png')
    t.equal(src.component, 'the-component')
    t.equal(src.componentTitle, 'The Component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.equal(src.relativeToModule, '../..')
    t.equal(src.area, 'assets')
    t.same(src.topicSegments, ['images'])
    t.equal(src.mediaType, 'image/png')
    t.equal(src.basename, 'the-file.png')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.png')
  })

  t.test('samples example', async (t) => {
    const src = getSourceFromPath('the-component', 'The Component', 'v1.2.3', 'modules/the-module/samples/the-file.xml')
    t.equal(src.component, 'the-component')
    t.equal(src.componentTitle, 'The Component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.equal(src.relativeToModule, '..')
    t.equal(src.area, 'samples')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'application/xml')
    t.equal(src.basename, 'the-file.xml')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.xml')
  })
})

test('find theme source using component, version and path', (t) => {

  t.plan(1)

  t.test('full content example', async (t) => {
    const src = getThemeSourceFromPath('default-theme', 'Default Theme', 'v42', 'scripts/the-script.js')
    t.equal(src.component, 'default-theme')
    t.equal(src.componentTitle, 'Default Theme')
    t.equal(src.version, 'v42')
    t.equal(src.module, 'scripts')
    t.equal(src.area, 'theme')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'application/javascript')
    t.equal(src.basename, 'the-script.js')
    t.equal(src.stem, 'the-script')
    t.equal(src.extname, '.js')
  })
})

test('find navigation source using component, version and path', (t) => {

  t.plan(1)

  t.test('simple example', async (t) => {
    const src = getNavigationSourceFromPath('the-component', 'The Component', 'v1.2.3', 'modules/the-module/the-nav.adoc')
    t.equal(src.component, 'the-component')
    t.equal(src.componentTitle, 'The Component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.equal(src.area, 'linked-navigation')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-nav.adoc')
    t.equal(src.stem, 'the-nav')
    t.equal(src.extname, '.adoc')
  })
})

test('find aspect navigation source using path', (t) => {

  t.plan(1)

  t.test('simple example', async (t) => {
    const src = getAspectNavigationSourceFromPath('nav/tutorials.adoc')
    t.equal(src.component, 'tutorials')
    t.equal(src.version, 'master')
    t.equal(src.module, 'ROOT')
    t.equal(src.area, 'aspect-navigation')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'tutorials.adoc')
    t.equal(src.stem, 'tutorials')
    t.equal(src.extname, '.adoc')
  })
})

test('find source from xref links', (t) => {

  t.plan(20)

  // * version + component + module + topic + file
  //   * version + component + module + file
  //     * version + component + file
  //       * version + file
  //       * component + file
  //     * version + module + file
  //       * version + file
  //       * module + file
  //     * component + module + file
  //       * component + file
  //       * module + file
  //   * version + component + topic + file
  //     * version + component + file
  //       * version + file
  //       * component + file
  //     * version + topic + file
  //       * version + file
  //       * topic + file
  //     * component + topic + file
  //       * component + file
  //       * topic + file
  //   * version + module + topic + file
  //     * version + module + file
  //       * version + file
  //       * module + file
  //     * version + topic + file
  //       * version + file
  //       * topic + file
  //     * module + topic + file
  //       * module + file
  //       * topic + file
  //   * component + module + topic + file
  //     * component + module + file
  //       * component + file
  //       * module + file
  //     * component + topic + file
  //       * component + file
  //       * topic + file
  //     * module + topic + file
  //       * module + file
  //       * topic + file

  // version + component + module + topic + file
  // version + component + module + file
  // version + component + topic + file
  // version + module + topic + file
  // component + module + topic + file
  // version + component + file
  // version + module + file
  // component + module + file
  // version + topic + file
  // component + topic + file
  // module + topic + file
  // version + file
  // component + file
  // module + file
  // topic + file
  // file

  t.test('version + component + module + topic + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-component:the-module:the-topic/the-subtopic/the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + component + module + topic + file with fragment', async (t) => {
    const src = getSourceFromXref('1.2.3@the-component:the-module:the-topic/the-subtopic/the-file#the-fragment')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
    t.equal(src.fragment, 'the-fragment')
  })

  t.test('version + module + topic + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-module:the-topic/the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('component + module + topic + file', async (t) => {
    const src = getSourceFromXref('the-component:the-module:the-topic/the-subtopic/the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'master')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + component + topic + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-component::the-topic/the-subtopic/the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'ROOT')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + component + module + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-component:the-module:the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('component + topic + file', async (t) => {
    const src = getSourceFromXref('the-component::the-topic/the-subtopic/the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'master')
    t.equal(src.module, 'ROOT')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('component + module + file', async (t) => {
    const src = getSourceFromXref('the-component:the-module:the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'master')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + topic + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-topic/the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'ROOT')
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + module + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-module:the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + component + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-component::the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'ROOT')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('module + topic + file', async (t) => {
    const src = getSourceFromXref('the-module:the-topic/the-subtopic/the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, undefined)
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('version + file', async (t) => {
    const src = getSourceFromXref('1.2.3@the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'ROOT')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('component + file', async (t) => {
    const src = getSourceFromXref('the-component::the-file')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'master')
    t.equal(src.module, 'ROOT')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('subtopic + file', async (t) => {
    const src = getSourceFromXref('the-topic/the-subtopic/the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, undefined)
    t.equal(src.module, undefined)
    t.same(src.topicSegments, ['the-topic', 'the-subtopic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('topic + file', async (t) => {
    const src = getSourceFromXref('the-topic/the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, undefined)
    t.equal(src.module, undefined)
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('module + file', async (t) => {
    const src = getSourceFromXref('the-module:the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, undefined)
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('file', async (t) => {
    const src = getSourceFromXref('the-file')
    t.equal(src.component, undefined)
    t.equal(src.version, undefined)
    t.equal(src.module, undefined)
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('file with #fragment', async (t) => {
    const src = getSourceFromXref('the-file#the-fragment')
    t.equal(src.component, undefined)
    t.equal(src.version, undefined)
    t.equal(src.module, undefined)
    t.same(src.topicSegments, [])
    t.equal(src.mediaType, 'text/asciidoc')
    t.equal(src.basename, 'the-file.adoc')
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
    t.equal(src.fragment, 'the-fragment')
  })

  t.test('empty xref throws Error', async (t) => {
    t.throws(() => {
      getSourceFromXref('')
    })
  })
})

test('extract aspect domain from xref', (t) => {
  t.plan(1)

  t.test('', async (t) => {
    let pageId, text, aspect
    [pageId, text, aspect] = extractAspectFromXref('the-page', 'Page Title, aspect=aspect-domain')
    t.equal(pageId, 'the-page')
    t.equal(text, 'Page Title')
    t.equal(aspect, 'aspect-domain')
  })
})

test('expand source using the source of a reference', (t) => {

  t.plan(3)

  t.test('', async (t) => {
    const src = {
      component: undefined,
      version: undefined,
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const referenceSrc = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-other-module',
      topicSegments: ['the-other-topic'],
      area: 'content',
      stem: 'the-other-file',
      extname: '.adoc',
    }
    expandSource(src, referenceSrc)
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.stem, 'the-file')
    t.equal(src.extname, '.adoc')
  })

  t.test('', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const referenceSrc = {
      component: 'the-aspect-domain',
      version: 'v1.2.3',
      module: 'the-other-module',
      topicSegments: ['the-other-topic'],
      area: 'aspect-navigation',
      stem: 'the-other-file',
      extname: '.adoc',
    }
    expandSource(src, referenceSrc)
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.stem, 'the-file')
    t.equal(src.aspect, 'the-aspect-domain')
    t.equal(src.extname, '.adoc')
  })

  t.test('', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const referenceSrc = {
      component: 'another-component',
      version: 'v4.0',
      module: 'the-other-module',
      topicSegments: ['the-other-topic'],
      area: 'content',
      stem: 'the-other-file',
      extname: '.adoc',
    }
    expandSource(src, referenceSrc, 'the-aspect-domain')
    t.equal(src.component, 'the-component')
    t.equal(src.version, 'v1.2.3')
    t.equal(src.module, 'the-module')
    t.same(src.topicSegments, ['the-topic'])
    t.equal(src.stem, 'the-file')
    t.equal(src.aspect, 'the-aspect-domain')
    t.equal(src.extname, '.adoc')
  })
})

test('find output path using source', (t) => {

  t.plan(23)

  t.test('full example', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-file.html')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example with aspect domain', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-domain~the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file.html')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example with aspect domain (and query strategy)', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component', 'default', 'query')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-domain~the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file.html?aspect=the-domain')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example from ROOT component', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'the-component')
    t.equal(output.dirname, '/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/1.2.3/the-module/the-topic/the-file.html')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../..')
  })

  t.test('full example with fragment', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      fragment: 'the-fragment',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-file.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file.html#the-fragment')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  // t.test('full example with query string', async (t) => {
  //   const src = {
  //     component: 'the-component',
  //     version: 'v1.2.3',
  //     module: 'the-module',
  //     topicSegments: ['the-topic'],
  //     area: 'content',
  //     stem: 'the-file',
  //     extname: '.adoc',
  //     aspect: 'the-aspect',
  //   }
  //   const output = getOutput(src, 'other-component')
  //   t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
  //   t.equal(output.basename, 'the-file.html')
  //   t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-file.html')
  //   t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file.html?aspect=the-aspect')
  //   t.equal(output.relativeToModule, '..')
  //   t.equal(output.relativeToRoot, '../../../..')
  // })
  //
  // t.test('full example with query string and fragment', async (t) => {
  //   const src = {
  //     component: 'the-component',
  //     version: 'v1.2.3',
  //     module: 'the-module',
  //     topicSegments: ['the-topic'],
  //     area: 'content',
  //     stem: 'the-file',
  //     extname: '.adoc',
  //     fragment: 'the-fragment',
  //     aspect: 'the-aspect',
  //   }
  //   const output = getOutput(src, 'other-component')
  //   t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
  //   t.equal(output.basename, 'the-file.html')
  //   t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-file.html')
  //   t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file.html?aspect=the-aspect#the-fragment')
  //   t.equal(output.relativeToModule, '..')
  //   t.equal(output.relativeToRoot, '../../../..')
  // })

  t.test('example with subtopic', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic', 'the-subtopic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic/the-subtopic')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-subtopic/the-file.html')
    t.equal(output.relativeToModule, '../..')
    t.equal(output.relativeToRoot, '../../../../..')
  })

  t.test('example without topic', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: [],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-file.html')
    t.equal(output.relativeToModule, '.')
    t.equal(output.relativeToRoot, '../../..')
  })

  t.test('example with ROOT module', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'ROOT',
      topicSegments: [],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-file.html')
    t.equal(output.relativeToModule, '.')
    t.equal(output.relativeToRoot, '../..')
  })

  t.test('example with master version', async (t) => {
    const src = {
      component: 'the-component',
      version: 'master',
      module: 'the-module',
      topicSegments: [],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/the-module')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/the-module/the-file.html')
    t.equal(output.relativeToModule, '.')
    t.equal(output.relativeToRoot, '../..')
  })

  t.test('example with ROOT module & master version from ROOT component', async (t) => {
    const src = {
      component: 'the-component',
      version: 'master',
      module: 'ROOT',
      topicSegments: [],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'the-component')
    t.equal(output.dirname, '/')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-file.html')
    t.equal(output.relativeToModule, '.')
    t.equal(output.relativeToRoot, '.')
  })

  t.test('example with assets/images', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['images'],
      area: 'assets',
      stem: 'the-file',
      extname: '.png',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/_images')
    t.equal(output.basename, 'the-file.png')
    t.equal(output.path, '/the-component/1.2.3/the-module/_images/the-file.png')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('example with assets/attachments', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['attachments'],
      area: 'assets',
      stem: 'archive',
      extname: '.zip',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/_attachments')
    t.equal(output.basename, 'archive.zip')
    t.equal(output.path, '/the-component/1.2.3/the-module/_attachments/archive.zip')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('example with assets/images with drop strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['images'],
      area: 'assets',
      stem: 'the-file',
      extname: '.png',
    }
    const output = getOutput(src, 'other-component', 'drop')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/_images')
    t.equal(output.basename, 'the-file.png')
    t.equal(output.path, '/the-component/1.2.3/the-module/_images/the-file.png')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('theme example', async (t) => {
    const src = {
      component: 'default-theme',
      version: 'v42',
      module: 'scripts',
      topicSegments: [],
      area: 'theme',
      stem: 'the-script',
      extname: '.js',
    }
    const output = getOutput(src, 'other-component')
    t.equal(output.dirname, '/_theme/scripts')
    t.equal(output.basename, 'the-script.js')
    t.equal(output.path, '/_theme/scripts/the-script.js')
    t.equal(output.relativeToModule, '.')
    t.equal(output.relativeToRoot, '../..')
  })

  t.test('full example with drop strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component', 'drop')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-file.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example (with aspect domain) with drop strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component', 'drop')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-domain~the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example (with aspect domain) on index page with drop strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'index',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component', 'drop')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-domain~index.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~index.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-domain~index')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example (with aspect domain) with drop strategy (and query strategy)', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component', 'drop', 'query')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'the-domain~the-file.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file?aspect=the-domain')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('index.html example with drop strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'index',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component', 'drop')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'index.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/index.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })

  t.test('full example with indexify strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component', 'indexify')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic/the-file')
    t.equal(output.basename, 'index.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-file/index.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file/')
    t.equal(output.relativeToModule, '../..')
    t.equal(output.relativeToRoot, '../../../../..')
  })

  t.test('full example (with aspect domain) with indexify strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component', 'indexify')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file')
    t.equal(output.basename, 'index.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file/index.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file/')
    t.equal(output.relativeToModule, '../..')
    t.equal(output.relativeToRoot, '../../../../..')
  })

  t.test('full example (with aspect domain) with indexify strategy (and query strategy)', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'the-file',
      extname: '.adoc',
      aspect: 'the-domain',
    }
    const output = getOutput(src, 'other-component', 'indexify', 'query')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file')
    t.equal(output.basename, 'index.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/the-domain~the-file/index.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/the-file/?aspect=the-domain')
    t.equal(output.relativeToModule, '../..')
    t.equal(output.relativeToRoot, '../../../../..')
  })

  t.test('index.html page with indexify strategy', async (t) => {
    const src = {
      component: 'the-component',
      version: 'v1.2.3',
      module: 'the-module',
      topicSegments: ['the-topic'],
      area: 'content',
      stem: 'index',
      extname: '.adoc',
    }
    const output = getOutput(src, 'other-component', 'indexify')
    t.equal(output.dirname, '/the-component/1.2.3/the-module/the-topic')
    t.equal(output.basename, 'index.html')
    t.equal(output.path, '/the-component/1.2.3/the-module/the-topic/index.html')
    t.equal(output.url, '/the-component/1.2.3/the-module/the-topic/')
    t.equal(output.relativeToModule, '..')
    t.equal(output.relativeToRoot, '../../../..')
  })
})
