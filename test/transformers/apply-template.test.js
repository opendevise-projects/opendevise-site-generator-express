'use strict'

const test = require('tap').test
const { createStream, applyTransformStream } = require('../utils')

const File = require('vinyl')
const applyTemplate = require('../../lib/transformers/apply-template')

test('template is applied on layout using templateModel', (t) => {

  t.plan(2)

  t.test('with two different templates', async (t) => {

    const templateModel = function (file) {
      return {
        title: () => 'The Title',
        contents: () => file.contents,
      }
    }

    const file = new File({
      contents: new Buffer('<h1>Foobar</h1>'),
    })

    const layouts = createStream([
      new File({
        path: 'foo/default.hbs',
        contents: new Buffer('<html>{{> head }}{{> primary-content }}</html>'),
      }),
    ])

    const partials = createStream([
      new File({
        path: 'bar/primary-content.hbs',
        contents: new Buffer('<body>{{{ contents }}}</body>'),
      }),
      new File({
        path: 'bar/head.hbs',
        contents: new Buffer('<head><title>{{ title }}</title></head>'),
      }),
    ])

    const expectedContents = '<html><head><title>The Title</title></head><body><h1>Foobar</h1></body></html>'

    const [transformedFile] = await applyTransformStream(applyTemplate({ layouts, partials, templateModel }), [file])
    t.equal(transformedFile.contents.toString(), expectedContents)
  })

  t.test('with custom layout', async (t) => {

    const templateModel = function (file) {
      return {
        title: () => '404',
        contents: () => file.contents,
      }
    }

    const file = new File({
      attributes: { 'page-layout': '404' },
      contents: new Buffer('<h1>404</h1>'),
    })

    const layouts = createStream([
      new File({
        path: 'layouts/404.hbs',
        contents: new Buffer('<html>{{> head }}{{> body }}</html>'),
      }),
    ])

    const partials = createStream([
      new File({
        path: 'partials/body.hbs',
        contents: new Buffer('<body>{{{ contents }}}</body>'),
      }),
      new File({
        path: 'partials/head.hbs',
        contents: new Buffer('<head><title>{{ title }}</title></head>'),
      }),
    ])

    const expectedContents = '<html><head><title>404</title></head><body><h1>404</h1></body></html>'

    const [transformedFile] = await applyTransformStream(applyTemplate({ layouts, partials, templateModel }), [file])
    t.equal(transformedFile.contents.toString(), expectedContents)
  })
})
