'use strict'

const test = require('tap').test
const { applyTransformStream } = require('../utils')

const File = require('vinyl')
const readAsciidoc = require('../../lib/transformers/read-asciidoc')
const convertAsciidoc = require('../../lib/transformers/convert-asciidoc')

test('AsciiDoc is converted to HTML', async (t) => {

  const file = new File({
    path: 'modules/bar/content/subpath/file.adoc',
    // mock asciidoctor convert
    ast: {
      convert() {
        return 'Some <strong>HTML</strong>'
      },
    },
  })

  const [transformedFile] = await applyTransformStream(convertAsciidoc(), [file])
  t.equal(transformedFile.contents.toString(), 'Some <strong>HTML</strong>')
  t.equal(file.extname, '.html')
})
