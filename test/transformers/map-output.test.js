'use strict'

const test = require('tap').test
const { applyTransformStream } = require('../utils')

const File = require('vinyl')
const mapOutput = require('../../lib/transformers/map-output')

test('path is set using out location', async (t) => {

  const file = new File({
    path: 'modules/the-module/content/the-topic/the-file.adoc',
    out: {
      path: '/modules/the-module/the-topic/the-file.html',
    },
  })

  const [transformedFile] = await applyTransformStream(mapOutput(), [file])
  t.equal(transformedFile.path, 'modules/the-module/the-topic/the-file.html')
})
