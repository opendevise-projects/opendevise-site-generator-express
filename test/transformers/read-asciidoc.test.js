'use strict'

const test = require('tap').test
const { createStream, applyTransformStream } = require('../utils')

const File = require('vinyl')
const readAsciidoc = require('../../lib/transformers/read-asciidoc')

test('asciidoc ast is correctly prepared', (t) => {

  t.plan(4)

  t.test('attributes are correct', async (t) => {

    const asciidocContents = 'This is *cool*!'

    const file = new File({
      path: 'modules/bar/content/file.adoc',
      contents: new Buffer(asciidocContents),
      coordinates: {
        modulePath: '/content/file.adoc',
      },
    })

    const [transformedFile] = await applyTransformStream(readAsciidoc(), [file])
    t.equal(transformedFile.attributes.docname, 'file')
    t.equal(transformedFile.attributes.docfilesuffix, '.adoc')
  })

  t.test('contents without any includes is correct', async (t) => {

    const asciidocContents = 'This is *cool*!'
    const expectedHtmlContents = '<div class="paragraph">\n<p>This is <strong>cool</strong>!</p>\n</div>'

    const file = new File({
      path: 'modules/bar/content/file.adoc',
      contents: new Buffer(asciidocContents),
      coordinates: {
        modulePath: '/content/file.adoc',
      },
    })

    const [transformedFile] = await applyTransformStream(readAsciidoc(), [file])
    t.equal(transformedFile.ast.convert(), expectedHtmlContents)
  })

  t.test('contents with an include is correct', async (t) => {

    const asciidocContents = 'This is *cool*!\n\ninclude::/samples/included-file.json[]'
    const expectedHtmlContents = '<div class="paragraph">\n<p>This is <strong>cool</strong>!</p>\n</div>\n<div class="paragraph">\n<p>{ "foobar": 42 }</p>\n</div>'

    const file = new File({
      path: 'modules/bar/content/file.adoc',
      foobar: 'foo',
      contents: new Buffer(asciidocContents),
    })

    const includes = createStream([
      new File({
        path: '/modules/bar/samples/included-file.json',
        modulePath: '/samples/included-file.json',
        foobar: 'bar',
        contents: new Buffer('{ "foobar": 77 }'),
      }),
      new File({
        path: '/modules/bar/samples/included-file.json',
        modulePath: '/samples/included-file.json',
        foobar: 'foo',
        contents: new Buffer('{ "foobar": 42 }'),
      }),
    ])

    function includeLocator(includedFile, fullTarget, sourceFile) {

      if (file.foobar !== includedFile.foobar) {
        return false
      }

      if (fullTarget !== includedFile.modulePath) {
        return false
      }

      return true
    }

    const [transformedFile] = await applyTransformStream(readAsciidoc({ includes, includeLocator }), [file])
    t.equal(transformedFile.ast.convert(), expectedHtmlContents)
  })

  t.test('contents with 2 recursive includes is correct', async (t) => {

    const parentAsciidocContents = '== Parent\n\ninclude::../child.adoc[]'
    const childAsciidocContents = '=== Child\n\ninclude::otherchild.adoc[]'
    const otherChildAsciidocContents = '=== Other Child\n\ninclude::../grandchild.adoc[]'
    const grandChildAsciidocContents = '==== Grand child\n\ninclude::../grandgrandchild.adoc[]'
    const grandGrandChildAsciidocContents = '===== Grand grand child\n\nHello!'
    const expectedHtmlContents = '<div class="sect1">\n<h2 id="parent"><a class="anchor" href="#parent"></a>Parent</h2>\n<div class="sectionbody">\n<div class="sect2">\n<h3 id="child"><a class="anchor" href="#child"></a>Child</h3>\n\n</div>\n<div class="sect2">\n<h3 id="other-child"><a class="anchor" href="#other-child"></a>Other Child</h3>\n<div class="sect3">\n<h4 id="grand-child"><a class="anchor" href="#grand-child"></a>Grand child</h4>\n<div class="sect4">\n<h5 id="grand-grand-child"><a class="anchor" href="#grand-grand-child"></a>Grand grand child</h5>\n<div class="paragraph">\n<p>Hello!</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>'

    const file = new File({
      path: 'modules/bar/content/parent/parent/parent/parent.adoc',
      contents: new Buffer(parentAsciidocContents),
    })

    const includes = createStream([
      new File({
        path: 'modules/bar/content/parent/parent/child.adoc',
        contents: new Buffer(childAsciidocContents),
      }),
      new File({
        path: 'modules/bar/content/parent/parent/otherchild.adoc',
        contents: new Buffer(otherChildAsciidocContents),
      }),
      new File({
        path: 'modules/bar/content/parent/grandchild.adoc',
        contents: new Buffer(grandChildAsciidocContents),
      }),
      new File({
        path: 'modules/bar/content/grandgrandchild.adoc',
        contents: new Buffer(grandGrandChildAsciidocContents),
      }),
    ])

    function includeLocator(includedFile, fullTarget, sourceFile) {

      if (fullTarget !== includedFile.path) {
        return false
      }

      return true
    }

    const [transformedFile] = await applyTransformStream(readAsciidoc({ includes, includeLocator }), [file])
    t.equal(transformedFile.ast.convert(), expectedHtmlContents)
  })
})
