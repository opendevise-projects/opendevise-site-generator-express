'use strict'

module.exports = {
  createStream,
  applyTransformStream,
}

const ReadableObjectStream = require('../lib/helpers/readable-object-stream')

function createStream(files) {
  const stream = new ReadableObjectStream()
  files.forEach((file) => stream.push(file))
  setImmediate(() => stream.emit('end'))
  return stream
}

function applyTransformStream(transform, files) {

  return new Promise((resolve, reject) => {

    const transformedFiles = []

    const stream = new ReadableObjectStream()
    stream.pipe(transform)
      .on('data', (file) => transformedFiles.push(file))
      .on('end', () => resolve(transformedFiles))

    files.forEach((file) => stream.push(file))
    setImmediate(() => stream.emit('end'))
  })
}
