'use strict'

const path = require('path')

module.exports = function (baseUrl, absUrl) {
  let relUrl = path.relative(baseUrl, absUrl)
  if (!relUrl.startsWith('.')) {
    relUrl = './' + relUrl
  }
  if (absUrl.endsWith('/') && !relUrl.endsWith('/')) {
    relUrl = relUrl + '/'
  }
  return relUrl
}
