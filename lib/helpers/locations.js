'use strict'

const _ = require('lodash')
const path = require('path')
const mime = require('./mime')

const FILE_PATH_REGEX = /^modules\/(.*)(\/(assets|content|samples)\/(?:(.+)\/)?)((.+)(\..+))$/
const THEME_PATH_REGEX = /^(.+)\/((.+)(\..+))$/
const NAVIGATION_PATH_REGEX = /^modules\/(.*)\/(?:(.+)\/)?((.+)(\..+))$/
const ASPECT_NAVIGATION_PATH_REGEX = /^nav\/(?:(.+)\/)?((.+)(\..+))$/
const XREF_REGEX = /^(?:(.+?)@)?(?:(?:(.+?):)?(?:(.+?))?:)?(?:(.+)\/)?(.+?)(?:#(.+?))?$/

module.exports = {

  getSourceFromPath(component, componentTitle, version, sourcePath) {

    const matches = FILE_PATH_REGEX.exec(sourcePath)
    if (!matches) {
      throw new Error(`file path ${sourcePath} is invalid`)
    }

    const [, module, pathToModule, area, topicString = '', basename, stem, extname] = matches
    const topicSegments = splitTopicSegments(topicString)
    const mediaType = mime.lookup(extname)
    const relativeToModule = path.relative(pathToModule, '/') || '.'

    return { component, componentTitle, version, module, relativeToModule, area, topicSegments, mediaType, basename, stem, extname }
  },

  getThemeSourceFromPath(component, componentTitle, version, sourcePath) {

    const matches = THEME_PATH_REGEX.exec(sourcePath)
    if (!matches) {
      throw new Error(`file path ${sourcePath} is invalid`)
    }

    const [, module, basename, stem, extname] = matches
    const area = 'theme'
    const topicSegments = []
    const mediaType = mime.lookup(extname)

    return { component, componentTitle, version, module, area, topicSegments, mediaType, basename, stem, extname }
  },

  getNavigationSourceFromPath(component, componentTitle, version, sourcePath) {

    const matches = NAVIGATION_PATH_REGEX.exec(sourcePath)
    if (!matches) {
      throw new Error(`file path ${sourcePath} is invalid`)
    }

    const [, module, topicString = '', basename, stem, extname] = matches
    const area = 'linked-navigation'
    const topicSegments = splitTopicSegments(topicString)
    const mediaType = mime.lookup(extname)

    return { component, componentTitle, version, module, area, topicSegments, mediaType, basename, stem, extname }
  },

  getAspectNavigationSourceFromPath(sourcePath) {

    const matches = ASPECT_NAVIGATION_PATH_REGEX.exec(sourcePath)
    if (!matches) {
      throw new Error(`file path ${sourcePath} is invalid`)
    }

    const [, topicString = '', basename, stem, extname] = matches
    const component = stem
    // TEMPORARY
    const componentTitle = stem
    const version = 'master'
    const module = 'ROOT'
    const area = 'aspect-navigation'
    const topicSegments = splitTopicSegments(topicString)
    const mediaType = mime.lookup(extname)

    return { component, componentTitle, version, module, area, topicSegments, mediaType, basename, stem, extname }
  },

  getSourceFromXref(xref, area = 'content', extname = '.adoc') {

    const matches = XREF_REGEX.exec(xref)
    if (!matches) {
      throw new Error(`file xref ${xref} is invalid`)
    }

    const [, extractedVersion, component, extractedModule, topicString = '', stem, fragment] = matches

    let version
    if (extractedVersion != null) {
      version = `v${extractedVersion}`
    }
    else if (component != null) {
      // if component is defined and version undefined, it implicitly means "master"
      version = 'master'
    }

    let module = extractedModule
    if ((component != null || version != null) && extractedModule == null) {
      // if component and/or version are defined and module undefined, it implicitly means "ROOT"
      module = 'ROOT'
    }

    const topicSegments = splitTopicSegments(topicString)
    const mediaType = mime.lookup(extname)
    const basename = stem + extname

    return { component, version, module, area, topicSegments, mediaType, basename, stem, extname, fragment }
  },

  extractAspectFromXref(pageId, text) {
    let aspect
    const commaIdx = text.indexOf(',')
    // xref:page.adoc#[Page, aspect=value]
    if (commaIdx > 0 && text.indexOf('aspect=', commaIdx + 1) > 0) {
      text = text.replace(/,\s*aspect=([^\s,]+)/, (_, m1) => { aspect = m1; return '' })
    }
    // xref:page.adoc?aspect=value#[Page]
    //else if (pageId.includes('.adoc?aspect=')) {
    //  pageId = pageId.replace(/\.adoc\?aspect=([^#&]+)/, (_, m1) => { aspect = m1; return '' })
    //}

    return [pageId, text, aspect]
  },

  expandSource(src, referenceSrc, aspect) {
    const newSource = _.defaults(src, referenceSrc)
    if (aspect) {
      newSource.aspect = aspect
    }
    else if (referenceSrc.area == 'aspect-navigation') {
      newSource.aspect = referenceSrc.component
    }
    return newSource
  },

  getOutput(src, siteRootComponent, htmlUrlExtensionStrategy = 'default', aspectPageUrlStrategy = 'path') {

    const component = (src.component === siteRootComponent) ? '' : src.component
    const version = (src.version !== 'master') ? src.version.replace(/^v/, '') : ''
    const module = (src.module === 'ROOT') ? '' : src.module

    const extname = (src.extname === '.adoc') ? '.html' : src.extname
    const prefixedStem = src.aspect ? `${src.aspect}~${src.stem}` : src.stem
    let prefixedBasename = prefixedStem + extname
    let basename = src.stem + extname
    const prefixedTopicSegments = [...src.topicSegments]
    const topicSegments = [...src.topicSegments]

    if (extname === '.html' && src.stem !== 'index' && htmlUrlExtensionStrategy === 'indexify') {
      prefixedBasename = 'index.html'
      basename = 'index.html'
      prefixedTopicSegments.push(prefixedStem)
      topicSegments.push(src.stem)
    }

    const queryString = (aspectPageUrlStrategy === 'query' && src.aspect != null) ? `?aspect=${src.aspect}` : ''
    const fragment = src.fragment ? `#${src.fragment}` : ''

    if (src.area === 'assets' && topicSegments.length > 0) {
      prefixedTopicSegments[0] = topicSegments[0] = `_${topicSegments[0]}`
    }

    let prefixedDirname = path.join(...['/', component, version, module, ...prefixedTopicSegments])
    let dirname = path.join(...['/', component, version, module, ...topicSegments])
    let pathToModule = path.join(...['/', component, version, module])

    if (src.area === 'theme') {
      prefixedDirname = path.join(...['/_theme', module])
      dirname = path.join(...['/_theme', module])
      pathToModule = path.join(...['/_theme', module])
    }

    const prefixedOutputPath = path.join(prefixedDirname, prefixedBasename)
    const outputPath = path.join(dirname, basename)

    let url = outputPath + fragment
    if (extname === '.html') {
      if (htmlUrlExtensionStrategy === 'default') {
        if (aspectPageUrlStrategy === 'query') {
          url = outputPath + queryString + fragment
        }
        else {
          url = prefixedOutputPath + queryString + fragment
        }
      }
      else if (htmlUrlExtensionStrategy === 'indexify') {
        if (aspectPageUrlStrategy === 'query') {
          url = path.join(dirname, '/') + queryString + fragment
        }
        else {
          url = path.join(prefixedDirname, '/') + queryString + fragment
        }
      }
      else if (htmlUrlExtensionStrategy === 'drop') {
        if (src.aspect) {
          if (aspectPageUrlStrategy === 'query') {
            url = path.join(dirname, src.stem === 'index' ? '/' : src.stem) + queryString + fragment
          }
          else {
            url = path.join(dirname, prefixedStem) + queryString + fragment
          }
        }
        else {
          url = path.join(dirname, src.stem === 'index' ? '/' : src.stem) + queryString + fragment
        }
      }
    }

    const relativeToModule = path.relative(prefixedDirname, pathToModule) || '.'
    const relativeToRoot = path.relative(prefixedDirname, '/') || '.'

    return {
      dirname: prefixedDirname,
      basename: prefixedBasename,
      path: prefixedOutputPath,
      url,
      relativeToModule,
      relativeToRoot,
    }
  },
}

function splitTopicSegments(topicString) {
  return topicString.split('/').filter((part) => part !== '')
}
