'use strict'

// vinyl already has a clone method but it doesn't like Opal's monkey patching
// this module is a simplified version of what Vinyl.proto.clone() offers with lodash cloneDeep() impl

const Vinyl = require('vinyl')
const cloneStats = require('clone-stats')
const _ = require('lodash')

module.exports = function (file) {

  const newFile = new Vinyl({
    cwd: file.cwd,
    base: file.base,
    stat: (file.stat ? cloneStats(file.stat) : null),
    history: file.history.slice(),
    contents: file.contents,
  })

  // Clone our custom properties
  Object.keys(file).forEach((key) => {
    if (Vinyl.isCustomProp(key)) {
      newFile[key] = _.cloneDeep(file[key])
    }
  })

  return newFile
}
