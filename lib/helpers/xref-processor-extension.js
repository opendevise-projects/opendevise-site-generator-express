'use strict'

const $$pageRefHandler = Symbol('$$pageRefHandler')

module.exports = class AsciidoctorXrefProcessorExtension {

  constructor(asciidoctor) {
    const thisExtension = this,
      Html5Converter = Opal.klass(asciidoctor.$$const.Converter, null, 'Html5Converter', () => {})

    Opal.alias(Html5Converter, 'super_inline_anchor', 'inline_anchor')

    Opal.defn(Html5Converter, '$inline_anchor', function (node) {
      if (node.getType() === 'xref') {
        // NOTE refId is undefined if document is self-referencing
        let refId = node.getAttribute('refid')
        if (node.getAttribute('path') || (refId && refId.endsWith('.adoc') && (refId = refId.slice(0, -5)) !== undefined)) {
          let text
          text = (text = node.$text()) === Opal.nil ? refId : text
          return thisExtension[$$pageRefHandler](refId, text)
        }
      }
      return this.$super_inline_anchor(node)
    })
  }

  onPageRef(callback) {
    this[$$pageRefHandler] = callback
  }
}
