'use strict'

const Readable = require('stream').Readable

class ReadableObjectStream extends Readable {
  constructor() {
    super({ objectMode: true })
  }

  _read(size) {
  }
}

module.exports = ReadableObjectStream
