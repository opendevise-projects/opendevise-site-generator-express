'use strict'

// master first then higher "vX.Y.Z" to lower "vX.Y.Z"
module.exports = function (a, b) {
  if (a === 'master') {
    return -1
  }
  if (b === 'master') {
    return 1
  }
  return -1 * a.localeCompare(b, 'en', { numeric: true })
}
