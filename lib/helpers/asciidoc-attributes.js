'use strict'

module.exports = function (file) {

  return {
    'env-site': '',
    imagesdir: file.out.relativeToModule + '/_images',
    attachmentsdir: file.out.relativeToModule + '/_attachments',
    samplesdir: file.src.relativeToModule + '/samples',
    fragmentsdir: file.src.relativeToModule + '/content/_fragments',
  }
}
