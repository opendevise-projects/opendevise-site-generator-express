'use strict'

const fs = require('fs')
const path = require('path')

const _ = require('lodash')
const del = require('del')
const git = require('nodegit')
const File = require('vinyl')
const yaml = require('js-yaml')
const vfs = require('vinyl-fs')
const map = require('map-stream')

const minimatchAll = require('./minimatch-all')
const ReadableObjectStream = require('./readable-object-stream')

module.exports = function (streamPatterns, { repositories, defaultBranchMatch = '*', localUrl = 'build/sources', componentDescFile, quiet = false }) {

  const allFilesStreams = Object.entries(streamPatterns)
    .map(([name]) => ({ [name]: new ReadableObjectStream() }))
    .reduce((a, b) => Object.assign({}, a, b), {})

  const allRepositories = repositories
    .map((repository) => getRepoUrlAndBranchMatch(repository, defaultBranchMatch))
    .map(async ({ repoUrl, branchMatch }) => {

      const { repository, isLocalRepo } = await openOrCloneRepository(localUrl, repoUrl, quiet)
      const isBare = repository.isBare()
      const references = await repository.getReferences(git.Reference.TYPE.OID)

      const allReferences = _(references)
        .map((branch) => getBranchInfo(branch))
        .groupBy('branchName')
        .mapValues((unorderedBranches) => {
          // isLocal comes from reference.isBranch() which is 0 or 1 so we'll end up with truthy isLocal last in the array
          const branches = _.sortBy(unorderedBranches, 'isLocal')
          return isLocalRepo ? _.last(branches) : _.first(branches)
        })
        .values()
        .filter(({ branchName }) => minimatchAll(branchName, branchMatch))
        .map(async ({ branch, branchName, isLocal, isHead }) => {

          const isWorkTree = !isBare && isLocalRepo && isHead
          if (!quiet) {
            console.log(`Using content from repository: ${repoUrl} @ ${branchName} (${isLocal ? 'local' : 'remote'}${isWorkTree ? ' work tree' : ' branch'})`)
          }

          if (isWorkTree) {
            const componentDesc = readComponentDescFromDisk(path.join(repoUrl, componentDescFile))
            const githubHttpsUrl = await getGithubHttpsUrl(repository)

            return new Promise((resolve, reject) => {
              vfs.src('**/*', { base: repoUrl, cwd: repoUrl })
                .pipe(map((file, next) => {
                  const streamsThatNeedTheFile = Object.entries(streamPatterns)
                    .filter(([name, patterns]) => minimatchAll(file.relative, patterns))
                    .map(([name]) => allFilesStreams[name])
                  if (streamsThatNeedTheFile.length > 0) {
                    file.path = file.relative
                    file.base = process.cwd()
                    file.cwd = process.cwd()
                    const githubEditUrl = getGithubEditUrl(githubHttpsUrl, branchName, file.path)
                    file.git = { workTree: repoUrl, remoteUrl: githubHttpsUrl, branchName, githubEditUrl }
                    file.componentDesc = componentDesc
                    streamsThatNeedTheFile.forEach((stream) => stream.push(file))
                  }
                  next(null, file)
                }))
                .on('end', resolve)
            })
          }
          else {
            const commit = await branch.repo.getBranchCommit(branch)
            const tree = await commit.getTree()

            const componentDesc = await readComponentDescFromTree(tree, componentDescFile)

            // we're done with a branch once we handled all its entries
            return getEntries(tree, async (entry) => {

              const streamsThatNeedTheFile = Object.entries(streamPatterns)
                .filter(([name, patterns]) => minimatchAll(entry.path(), patterns))
                .map(([name]) => allFilesStreams[name])

              if (streamsThatNeedTheFile.length > 0) {
                const file = await createVinylFile({ entry, repoUrl, branchName })
                file.componentDesc = componentDesc
                streamsThatNeedTheFile.forEach((stream) => stream.push(file))
              }
            })
          }
        })
        .value()

      // we're done with a repository once we handled all its references
      return Promise.all(allReferences)
      // nodegit repositories need to be manually closed
        .then(() => repository.free())
    })

  // Once we have all repositories revolved, it means we can close the stream
  Promise.all(allRepositories).then(() => {
    Object.values(allFilesStreams).forEach((stream) => stream.emit('end'))
  })

  return allFilesStreams
}

const getFetchOptions = () => {
  let sshKeyAuthAttempted
  return {
    callbacks: {
      // https://github.com/nodegit/nodegit/blob/master/guides/cloning/ssh-with-agent/README.md#github-certificate-issue-in-os-x
      certificateCheck: () => 1,
      credentials: (_, username) => {
        if (sshKeyAuthAttempted) {
          throw new Error('Failed to authenticate git client using SSH key; SSH agent is not running')
        }
        else {
          sshKeyAuthAttempted = true
          return git.Cred.sshKeyFromAgent(username)
        }
      },
    },
  }
}

function getRepoUrlAndBranchMatch(repositorySpec, defaultBranchMatch) {
  const { repository, branches } = _.isString(repositorySpec) ? {
    repository: repositorySpec,
    branches: null,
  } : repositorySpec
  const branchMatch = branches || defaultBranchMatch
  return { repoUrl: repository, branchMatch }
}

async function openOrCloneRepository(localUrl, repoUrl, quiet) {

  const isLocalRepo = isLocalDirectory(repoUrl)

  let localPath
  let repository
  let bare

  if (isLocalRepo) {
    localPath = repoUrl
    bare = !isLocalDirectory(path.join(localPath, '.git'))
  }
  else {
    localPath = localUrl + '/' + repoUrl.replace(/[:/\\]+/g, '__')
    bare = true
  }

  try {
    if (bare) {
      repository = await git.Repository.openBare(localPath)
      if (!isLocalRepo) {
        // fetches new branches and deletes old local ones
        await repository.fetch('origin', Object.assign({ prune: 1 }, getFetchOptions()))
      }
    }
    else {
      repository = await git.Repository.open(localPath)
    }
  } catch (e) {
    if (!isLocalRepo) {
      if (!quiet) {
        console.log(`Cloning repository at ${repoUrl} ...`)
      }
      del.sync(localPath)
      repository = await git.Clone.clone(repoUrl, localPath, { bare: 1, fetchOpts: getFetchOptions() })
    }
  }

  return { repository, isLocalRepo }
}

function getBranchInfo(branch) {
  return {
    branch,
    branchName: branch.shorthand().replace(/^origin\//, ''),
    isLocal: branch.isBranch(),
    isHead: branch.isHead(),
  }
}

function readComponentDescFromDisk(path) {
  return yaml.safeLoad(fs.readFileSync(path), 'UTF-8')
}

async function readComponentDescFromTree(tree, path) {
  const descEntry = await tree.getEntry(path)
  const descBlob = await descEntry.getBlob()
  return yaml.safeLoad(descBlob.content().toString())
}

function getEntries(tree, onEntry) {

  return new Promise(async (resolve) => {

    const promisedEntries = []

    const walker = tree.walk()
    walker.on('entry', (entry) => {
      promisedEntries.push(onEntry(entry))
    })
    walker.on('end', (entries) => resolve(Promise.all(promisedEntries)))
    walker.start()
  })
}

async function createVinylFile({ entry, repoUrl, branchName }) {

  const blob = await entry.getBlob()
  const contents = blob.content()

  const stat = new fs.Stats({})
  stat.mode = entry.filemode()
  stat.size = contents.length

  const githubEditUrl = getGithubEditUrl(repoUrl, branchName, entry.path())

  return new File({
    path: entry.path(),
    contents,
    stat,
    git: { remoteUrl: repoUrl, branchName, githubEditUrl },
  })
}

function isLocalDirectory(path) {
  try {
    return fs.lstatSync(path).isDirectory()
  }
  catch (e) {
    return false
  }
}

async function getGithubHttpsUrl(repository) {
  const remote = await repository.getRemote('origin')
  return remote.url().replace('git@github.com:', 'https://github.com/')
}

function getGithubEditUrl(remoteUrl, branch, filepath) {
  const githubUrl = remoteUrl.replace(/\.git$/, '')
  return `${githubUrl}/edit/${branch}/${filepath}`
}
