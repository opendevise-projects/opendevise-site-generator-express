'use strict'

const path = require('path')
const fs = require('fs')

const map = require('map-stream')
const glob = require('glob-promise')
const vfs = require('vinyl-fs')
const minimatchAll = require('./minimatch-all')
const download = require('download')
const zip = require('gulp-vinyl-zip')
const buffer = require('gulp-buffer')
const url = require('url')
const ReadableObjectStream = require('./readable-object-stream')

const github = require('github')()

module.exports = function (streamPatterns, { repository, tag, skipThemeCache, themeArchive, quiet = false }) {

  const allFilesStreams = Object.entries(streamPatterns)
    .map(([name]) => ({ [name]: new ReadableObjectStream() }))
    .reduce((a, b) => Object.assign({}, a, b), {})

  const { owner, repo } = parseGitHubUrl(repository)

  getThemeBundle({ owner, repo, tag, skipThemeCache, themeArchive })
    .then((themeBundlePath) => {

      if (!quiet) {
        console.log(`Loading theme bundle from GitHub release: ${themeBundlePath}`)
      }

      vfs.src(themeBundlePath)
        .pipe(zip.src())
        .pipe(buffer())
        .pipe(map((file, next) => {

          Object.entries(streamPatterns)
            .filter(([name, patterns]) => minimatchAll(file.path, patterns))
            .map(([name]) => allFilesStreams[name])
            .forEach((stream) => stream.push(file))

          next(null, file)
        }))
        .on('end', () => {
          Object.values(allFilesStreams).forEach((stream) => stream.emit('end'))
        })
    })
    .catch((e) => console.error(e))

  return allFilesStreams
}

function parseGitHubUrl(githubUrl) {
  const [, owner, repo] = (/https:\/\/github\.com\/(.*)\/(.*)/.exec(githubUrl) || [])
  return { owner, repo }
}

async function getThemeBundle({ owner, repo, tag, skipThemeCache, themeArchive }) {

  if (themeArchive != '') {
    return path.resolve(process.cwd(), themeArchive)
  }

  if (!skipThemeCache) {

    if (tag) {
      const cachedBundle = `build/themes/${repo}-${tag}.zip`
      if (fs.existsSync(cachedBundle)) {
        return cachedBundle
      }
    }
    else {
      const cachedBundles = await glob(`build/themes/${repo}-v*.zip`)
      const greatestVersion = cachedBundles
        .map((filepath) => {
          const match = /v(\d+)\.zip$/.exec(filepath)
          return match ? Number(match[1]) : -1
        })
        .reduce((a, b) => Math.max(a, b), -1)

      if (greatestVersion > -1) {
        return `build/themes/${repo}-v${greatestVersion}.zip`
      }
    }
  }

  return getRemoteThemeBundle({ owner, repo, tag })
}

async function getRemoteThemeBundle({ owner, repo, tag }) {
  if (process.env.GITHUB_TOKEN) {
    github.authenticate({ type: 'token', token: process.env.GITHUB_TOKEN })
  }
  // TODO show a friendly error if bundle can't be found or downloaded
  const { data: release } = await (tag ? github.repos.getReleaseByTag({ owner, repo, tag }) : github.repos.getLatestRelease({ owner, repo }))
  const { data: releaseAssets } = await github.repos.getAssets({ owner, repo, id: release.id })
  const releaseBundleAsset = releaseAssets.find((asset) => asset.name === `${repo}-${release.name}.zip`)
  const { path: filepath } = url.parse(releaseBundleAsset.browser_download_url)
  const { base } = path.parse(filepath)
  await download(releaseBundleAsset.browser_download_url, 'build/themes')
  return path.join('build/themes', base)
}
