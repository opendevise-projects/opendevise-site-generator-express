'use strict'

const _ = require('lodash')
const path = require('path')
const url = require('url')

const getRelativeUrl = require('../helpers/relative-url')

module.exports = function (navigation, siteTitle, siteUrl, siteRootComponent, siteAspectDomain, swiftypeKey) {

  return async (file) => {

    const isSiteAspectDomain = (file.src.component === siteAspectDomain)

    const { navigationIndex, domainVersionIndex, siteRootUrl } = await navigation
    const absoluteSiteAspectNav = siteAspectDomain ? _.cloneDeep(_.get(navigationIndex, [siteAspectDomain, 'master'], [])) : undefined
    const absoluteNav = isSiteAspectDomain ? undefined : _.cloneDeep(_.get(navigationIndex, [file.src.component, file.src.version], []))

    // this step also adds "currentPage" and "currentPath" to absoluteNav or absoluteSiteAspectNav
    const absoluteBreadcrumbs = getBreadcrumb(file.pub.url, isSiteAspectDomain ? absoluteSiteAspectNav : absoluteNav)
    absoluteBreadcrumbs.forEach((segment, i, allSegments) => {
      if (i === allSegments.length - 1) {
        segment.currentPage = true
      }
      else {
        segment.currentPath = true
      }
    })

    const siteAspectNav = siteAspectDomain ? relativizeUrls(file.out.dirname, absoluteSiteAspectNav) : undefined
    const pageNav = isSiteAspectDomain ? undefined : relativizeUrls(file.out.dirname, absoluteNav)
    const filteredDomainVersionIndex = domainVersionIndex.filter((domain) => domain.name !== siteAspectDomain)
    const pageDomainVersionIndex = relativizeUrls(file.out.dirname, filteredDomainVersionIndex)
    const pageVersions = relativizeUrls(file.out.dirname, file.rel.versions)
    const breadcrumbs = relativizeUrls(file.out.dirname, absoluteBreadcrumbs)

    // first breadcrumb can be empty if navigation has no title
    if (breadcrumbs[0] && !breadcrumbs[0].title) {
      breadcrumbs.shift()
    }
    // remove redundant first breadcrumb when it's the same as the component title
    // if (breadcrumbs[0] != null && breadcrumbs[0].title === file.src.componentTitle) {
    //   breadcrumbs.shift()
    // }

    let domainVersions, defaultDomainVersion
    // FIXME the root component is missing from the pageDomainVersionIndex
    pageDomainVersionIndex.forEach((d) => {
      if (d.name === file.src.component) {
        d.selected = true
        // FIXME the default version of a component should be configurable
        defaultDomainVersion = d.versions[0]
        const version = (domainVersions = d.versions).find((v) => v.string === file.src.version)
        if (version != null) {
          version.selected = true
        }
      }

      const greatestVersion = d.versions[0]
      // FIXME conversion from master to latest should probably be done in template
      if (greatestVersion.string === 'master') {
        // QUESTION should we set latest flag if string is latest?
        greatestVersion.string = 'latest'
      }
      else {
        greatestVersion.latest = true
      }
    })

    const siteRootPath = siteRootUrl ? (path.relative(file.out.dirname, siteRootUrl) || './') : null
    let themeRootPath
    // TODO siteRootPath should also be absolute on 404 page
    if (file.attributes['page-layout'] === '404' && siteUrl && !siteUrl.startsWith('file://')) {
      try {
        themeRootPath = path.join(url.parse(siteUrl).pathname, '_theme')
      }
      catch (e) {
        themeRootPath = file.out.relativeToRoot + '/_theme'
      }
    }
    else {
      themeRootPath = file.out.relativeToRoot + '/_theme'
    }

    return {
      site: {
        url: siteUrl,
        title: siteTitle,
        // Q: should we add a new top-level variable to store CI info?
        buildNumber: (process.env.TRAVIS_BUILD_NUMBER || process.env.BUILD_NUMBER),
        domains: pageDomainVersionIndex,
        //root: siteRootComponent,
        aspect: siteAspectDomain,
        aspectNav: siteAspectNav,
        swiftypeKey,
      },
      title: file.attributes.doctitle,
      contents: file.contents,
      // TODO add path of current page
      description: file.attributes.description,
      keywords: file.attributes.keywords,
      domain: {
        name: file.src.component,
        siteAspect: isSiteAspectDomain,
        title: file.src.componentTitle,
        type: (file.aspectDomainPage ? 'aspect' : 'component'),
        versioned: file.src.version !== 'master',
        url: (defaultDomainVersion ? defaultDomainVersion.url : siteRootPath),
        root: !file.aspectDomainPage && file.src.component === siteRootComponent,
        // should we have a flag to indicate there's more than one version in this domain?
        // QUESTION should version be a top-level variable?
        // QUESTION rename to currentVersion?
        version: {
          //latest: true,
          string: file.src.version,
        },
        versions: domainVersions,
      },
      // QUESTION should this be otherVersions?
      versions: pageVersions,
      breadcrumbs,
      navigation: pageNav,
      canonicalUrl: file.pub.canonicalUrl,
      editUrl: file.git.githubEditUrl,
      themeRootPath,
      siteRootUrl: siteRootPath,
      home: file.pub.url === siteRootUrl,
    }
  }
}

function getBreadcrumb(url, pageNavigation, parents = []) {

  const currentPage = pageNavigation.find((navItem) => navItem.url === url)

  if (currentPage != null) {
    return [...parents, currentPage]
  }

  return _.flattenDeep(pageNavigation
    .map((navItem) => getBreadcrumb(url, navItem.items, [...parents, navItem])))
}

function relativizeUrls(dirname, object) {
  return _.cloneDeepWith(object, (value, key) => {
    if (key === 'url' && value && !hasProtocol(value)) {
      return getRelativeUrl(dirname, value)
    }
  })
}

function hasProtocol(url) {
  return url.startsWith('http://') || url.startsWith('https://')
}
