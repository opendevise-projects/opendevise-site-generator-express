'use strict'

module.exports = function (includedFile, fullTarget, sourceFile) {

  if (sourceFile.src.component !== includedFile.src.component) {
    return false
  }

  if (sourceFile.src.version !== includedFile.src.version) {
    return false
  }

  if (sourceFile.src.module !== includedFile.src.module) {
    return false
  }

  if (includedFile.path !== fullTarget) {
    return false
  }

  return true
}
