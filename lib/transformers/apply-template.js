'use strict'

const map = require('map-stream')
const streamToArray = require('stream-to-array')
const through2 = require('through2')
const handlebars = require('handlebars')
const requireFromString = require('require-from-string')

async function registerHelpers(helpersStream) {
  const helpers = await streamToArray(helpersStream)
  return helpers
    .forEach((file) => {
      const helperFunction = requireFromString(file.contents.toString())
      handlebars.registerHelper(file.stem, helperFunction)
    })
}

async function compileLayouts(layoutsStream) {
  const layouts = await streamToArray(layoutsStream)
  return layouts
    .map((file) => ({ [file.basename]: handlebars.compile(file.contents.toString(), { preventIndent: true }) }))
    .reduce((a, b) => Object.assign({}, a, b), {})
}

async function registerPartials(partialsStream) {
  const partials = await streamToArray(partialsStream)
  return partials
    .forEach((file) => {
      handlebars.registerPartial(file.stem, file.contents.toString())
    })
}

module.exports = function ({ helpers, layouts, partials, templateModel }) {

  const compileAndRegisterTemplates = Promise.all([
    compileLayouts(layouts),
    registerHelpers(helpers),
    registerPartials(partials),
  ])

  return map(async (file, next) => {

    const [compiledLayouts] = await compileAndRegisterTemplates

    // assume page layout is default.hbs unless specified otherwise
    const templateName = ((file.attributes || {})['page-layout'] || 'default') + '.hbs'

    const compiledLayout = compiledLayouts[templateName]

    if (!compiledLayout) {
      throw new Error(`Template ${templateName} could not be found in`, compiledLayouts)
      process.exit(1)
    }

    const model = await templateModel(file)
    const newContents = new Buffer(compiledLayout(model))

    file.contents = newContents
    next(null, file)
  })
}
