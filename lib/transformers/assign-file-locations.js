'use strict'

const map = require('map-stream')
const { getSourceFromPath, getThemeSourceFromPath, getNavigationSourceFromPath, getAspectNavigationSourceFromPath, getOutput } = require('../helpers/locations')

module.exports = function ({ siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy, type }) {

  return map((file, next) => {

    if (type === 'theme') {
      file.src = getThemeSourceFromPath('unknown-theme', 'Unknown Theme', 'unknown-version', file.path)
    }
    else if (type === 'linked-navigation') {
      file.src = getNavigationSourceFromPath(file.componentDesc.name, file.componentDesc.title, file.componentDesc.version, file.path)
    }
    else if (type === 'aspect-navigation') {
      file.src = getAspectNavigationSourceFromPath(file.relative)
    }
    else {
      file.src = getSourceFromPath(file.componentDesc.name, file.componentDesc.title, file.componentDesc.version, file.path)
    }

    const out = getOutput(file.src, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy)
    file.pub = { url: out.url, absoluteUrl: siteUrl + out.url }
    delete out.url
    file.out = out
    file.rel = {}

    next(null, file)
  })
}
