'use strict'

const fs = require('fs')
const path = require('path')
const map = require('map-stream')
const streamToArray = require('stream-to-array')
const asciidoctor = require('asciidoctor.js')()

const { getSourceFromPath, getSourceFromXref, extractAspectFromXref, expandSource, getOutput } = require('../helpers/locations')
const getRelativeUrl = require('../helpers/relative-url')

const AsciidoctorIncludeProcessorExtension = require('../helpers/include-processor-extension')
const AsciidoctorXrefProcessorExtension = require('../helpers/xref-processor-extension')

const includeProcessor = new AsciidoctorIncludeProcessorExtension(asciidoctor)
const xrefProcessor = new AsciidoctorXrefProcessorExtension(asciidoctor)

const asciidoctorOptions = {
  safe: 'safe',
  doctype: 'article',
  header_footer: false,
  attributes: {
    'source-highlighter': 'highlight.js',
    'toc!': '',
    sectanchors: '',
    idprefix: '',
    idseparator: '-',
    icons: 'font',
  },
}

module.exports = function ({ silent = false, attributes, includes, includeLocator, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy } = {}) {

  const includesArrayPromise = streamToArray(includes)

  return map(async (file, next) => {

    const opts = Object.assign({}, asciidoctorOptions, {})
    const fileAttributes = attributes ? attributes(file) : {}
    opts.attributes = Object.assign({}, opts.attributes, fileAttributes, {
      docname: file.stem,
      docfile: file.path,
      docfilesuffix: file.extname,
    })

    const includesArray = await includesArrayPromise
    includeProcessor.onInclude((doc, target, cursor) => {

      const parsedPath = path.parse(cursor.file)
      let resolvedTarget = target
      let includeContents

      // NOTE aspect navigation files are located in the working directory
      if (file.src && file.src.area === 'aspect-navigation') {
        if (!path.isAbsolute(resolvedTarget)) {
          resolvedTarget = path.resolve(parsedPath.dir, target)
        }
        if (fs.existsSync(resolvedTarget)) {
          includeContents = fs.readFileSync(resolvedTarget, 'UTF-8')
        }
      }
      else {
        if (!path.isAbsolute(resolvedTarget)) {
          resolvedTarget = path.resolve(path.join('/', parsedPath.dir), target).slice(1)
        }
        const includeFile = includesArray.find((candidate) => includeLocator(candidate, resolvedTarget, file))
        if (includeFile) {
          includeContents = includeFile.contents.toString()
        }
      }

      if (includeContents) {
        return {
          contents: includeContents,
          file: resolvedTarget,
          path: parsedPath.base,
        }
      }
    })

    xrefProcessor.onPageRef((refId, text) => {
      if (refId) {
        let aspect
        [refId, text, aspect] = extractAspectFromXref(refId, text)
        const refSource = getSourceFromXref(refId)
        expandSource(refSource, file.src, aspect)
        const refOutput = getOutput(refSource, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy)

        const url = file.src.area == 'linked-navigation' || file.src.area == 'aspect-navigation' ?
          refOutput.url : getRelativeUrl(file.out.dirname, refOutput.url)

        return `<a href="${url}">${text}</a>`
      }
      else {
        console.warn(`target of xref macro is empty at component: ${file.src.component}; branch: ${file.git.branchName}; path: ${file.path}`)
        return text
      }
    })

    // Trap errors and warnings from Asciidoctor and either aggregate or suppress
    const resetStdErrWriter = hijackStdErrWriter(file, silent)

    const asciidoc = file.contents.toString()
    file.ast = asciidoctor.load(asciidoc, opts)
    file.attributes = file.ast.getAttributes()

    // Reset the classic stderr write behaviour
    resetStdErrWriter()

    next(null, file)
  })
}

const ASCIIDOCTOR_MESSAGE = /^asciidoctor: (WARNING|ERROR): (?:(?:[^:]+\.adoc: )?line ([0-9]+): )?(.*)(?:\n|$)/
function hijackStdErrWriter(file, silent) {

  let firstError = true, filePath = file.path

  if (file.git) {
    if (file.git.workTree) {
      filePath = `${file.git.workTree}/${file.path}`
    }
    else {
      filePath = `${file.git.remoteUrl} : ${file.path} @ ${file.git.branchName}`
    }
  }

  const stdErrWriter = process.stderr.write
  process.stderr.write = (stdErrMessage) => {

    const [, type, line, message] = ASCIIDOCTOR_MESSAGE.exec(stdErrMessage) || []

    // Asciidoctor messages
    if (type) {
      if (!silent) {
        if (firstError) {
          stdErrWriter.call(process.stderr, `[asciidoctor] ${filePath}\n`)
        }

        const typeCol = ' '.repeat(9 - type.length) + type
        const lineCol = ' '.repeat(6 - (line || '?').length) + (line || '?')
        const messageCol = ' - ' + message
        stdErrWriter.call(process.stderr, `${typeCol}${lineCol}${messageCol}\n`)
        firstError = false
      }
    }
    // All other messages
    else {
      return stdErrWriter.call(process.stderr, stdErrMessage)
    }
  }

  // return a function to reset normal stderr callback
  return () => {
    process.stderr.write = stdErrWriter
  }
}
