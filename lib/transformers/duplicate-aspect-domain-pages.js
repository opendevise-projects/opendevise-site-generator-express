'use strict'

const path = require('path')
const through2 = require('through2')

const cloneVinyl = require('../helpers/clone-vinyl')
const { getOutput } = require('../helpers/locations')

module.exports = function (navigation, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy) {

  return through2.obj(async function (file, enc, next) {

    const { indexedAspectPages } = await navigation

    if (indexedAspectPages[file.pub.url]) {
      indexedAspectPages[file.pub.url].forEach(({ component, componentTitle }) => {
        const newFile = cloneVinyl(file)
        newFile.src.aspect = component
        const out = getOutput(newFile.src, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy)
        newFile.pub = { url: out.url, canonicalUrl: file.pub.canonicalUrl }
        delete out.url
        newFile.out = out
        newFile.src.component = component
        newFile.src.componentTitle = componentTitle
        newFile.src.version = 'master'
        newFile.aspectDomainPage = true
        this.push(newFile)
      })
    }

    this.push(file)
    next()
  })
}
