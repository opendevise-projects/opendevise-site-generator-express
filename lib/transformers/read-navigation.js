'use strict'

const fs = require('fs')

const _ = require('lodash')
const through2 = require('through2')
const File = require('vinyl')

const sortByVersion = require('../helpers/sort-by-version')
const { getOutput } = require('../helpers/locations')

module.exports = function ({ nav, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy }) {

  const navigationIndex = {}
  const indexedAspectPages = {}
  const domainVersions = []

  return through2.obj(
    function (file, enc, next) {

      const { component, componentTitle, version } = file.src
      const isAspectDomain = (file.src.area === 'aspect-navigation')

      const mainList = file.ast.blocks[0]
      const navigation = extractNavigation(mainList.getTitle(), mainList.blocks)

      const idx = isAspectDomain ? 0 : file.componentDesc.nav.indexOf(file.relative)

      if (navigationIndex[component] == null) {
        navigationIndex[component] = {}
      }
      if (navigationIndex[component][version] == null) {
        navigationIndex[component][version] = []
      }
      navigationIndex[component][version][idx] = navigation

      if (isAspectDomain) {
        const aspectDomain = { component, componentTitle: navigation.title }
        const pages = getPages([navigation]).forEach((aspectUrl) => {
          // FIXME this logic should be located in locations helper
          let url
          if (aspectPageUrlStrategy === 'query') {
            url = aspectUrl.slice(0, aspectUrl.indexOf('?'))
          }
          else if (htmlUrlExtensionStrategy === 'drop') {
            url = aspectUrl.slice(0, aspectUrl.lastIndexOf('/') + 1) +
                (aspectUrl.endsWith('~index') ? '' : aspectUrl.slice(aspectUrl.lastIndexOf('~') + 1))
          }
          else {
            // TODO don't use regexp here
            url = aspectUrl.replace(/\/[^\/]+~/, '/')
          }
          const aspectPagesForUrl = indexedAspectPages[url]
          if (!aspectPagesForUrl) {
            indexedAspectPages[url] = [aspectDomain]
          }
          else if (!aspectPagesForUrl.includes(aspectDomain)) {
            aspectPagesForUrl.push(aspectDomain)
          }
        })
        domainVersions.push({
          name: component,
          type: 'aspect',
          title: (file.src.componentTitle = navigation.title),
          version,
          url: (navigation.url || navigation.items[0].url),
        })
      }
      else {
        domainVersions.push({
          name: component,
          type: 'component',
          title: componentTitle,
          version,
          url: getIndexUrl(component, siteRootComponent, version, htmlUrlExtensionStrategy, aspectPageUrlStrategy),
        })
      }

      // We don't need the AST anymore so let's help the garbage collector
      delete file.ast

      next()
    },
    function (callback) {

      let siteRootUrl

      const domainVersionIndex = _(domainVersions)
        .sort((a, b) => {
          if (a.title === b.title) {
            return sortByVersion(a.version, b.version)
          }
          return a.title.localeCompare(b.title)
        })
        .groupBy('name')
        .map((versionsForDomain, name) => {
          // FIXME the default version of a component should be configurable
          const latestVersion = versionsForDomain[0]
          return {
            name,
            type: latestVersion.type,
            title: latestVersion.title,
            url: latestVersion.url,
            versions: _(versionsForDomain)
              .map((v) => ({ string: v.version, url: v.url }))
              .sortedUniqBy('string')
              .value(),
          }
        })
        // remove siteRootComponent from index but keep its latest version as "root" URL
        .filter(({ name, versions }) => {
          if (name !== siteRootComponent) {
            return true
          }
          siteRootUrl = versions[0].url
        })
        .value()

      const navigation = { navigationIndex, indexedAspectPages, domainVersionIndex, siteRootUrl }
      const contents = process.env.DEBUG === 'true' ? JSON.stringify(navigation, null, '  ') : ''

      const navigationFile = new File({
        navigation,
        path: 'navigation.json',
        contents: new Buffer(contents),
        stat: new fs.Stats({}),
      })

      this.push(navigationFile)
      callback()
    })
}

function extractNavigation(titleOrLink, items) {

  const { title, url } = extractLink(titleOrLink)

  const extractedNavigationItems = items.map((item) => {
    let subItems = []
    if (item.blocks.length === 1 && item.blocks[0].node_name === 'ulist') {
      subItems = item.blocks[0].blocks
    }
    return extractNavigation(item.$text(), subItems)
  })

  return { title, url, items: extractedNavigationItems }
}

function extractLink(html) {
  const match = /href="(.*?)">(.*)</.exec(html)
  return match ? { url: match[1], title: match[2] } : { title: html }
}

function getPages(navigationItems) {
  return _(navigationItems)
    .map((navItem) => [navItem.url, ...getPages(navItem.items)])
    .flattenDeep()
    .compact()
    .value()
}

function getIndexUrl(component, siteRootComponent, version, htmlUrlExtensionStrategy, aspectPageUrlStrategy) {
  const { url } = getOutput({
    component,
    version,
    module: 'ROOT',
    topicSegments: [],
    area: 'content',
    stem: 'index',
    extname: '.adoc',
  }, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy)
  return url
}
