'use strict'

const File = require('vinyl')
const through2 = require('through2')

module.exports = function () {

  const tree = {}

  return through2.obj(
    function (file, enc, next) {

      if (file.git == null || file.src == null || file.src.extname !== '.adoc') {
        return next(null, file)
      }

      const allPaths = [
        file.git.location,
        file.git.branchName,
        ...file.history[0].split('/'),
      ]

      allPaths.reduce((newTree, path, i, allPaths) => {

        if (newTree[path] == null) {
          newTree[path] = {}
        }

        return newTree[path]
      }, tree)

      next(null, file)
    },
    function (callback) {

      const lines = recursiveTree(tree, [])

      this.push(new File({
        path: 'vinyl-collection-report.txt',
        contents: new Buffer(lines),
      }))

      callback()
    })
}

function recursiveTree(tree, lines, depth = 0) {

  const allEntries = Object.keys(tree)
  allEntries.forEach((entry) => {
    const childDepth = Object.keys(tree[entry]).length
    lines.push(tab(depth * 4) + entry + (childDepth ? '/' : ''))
    recursiveTree(tree[entry], lines, depth + 1)
  })

  return lines.join('\n')
}

function tab(size) {
  return Array
    .from(new Array(size + 1))
    .join(' ')
}
