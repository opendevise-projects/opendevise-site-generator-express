'use strict'

const File = require('vinyl')
const through2 = require('through2')
const sm = require('sitemap')

const sortByVersion = require('../helpers/sort-by-version')

module.exports = function ({ siteUrl }) {

  const sitemaps = {}
  const lastmodISO = new Date().toISOString()

  return through2.obj(
    function (file, enc, next) {

      // skipping duplicated pages from aspect domains
      if (file.aspectDomainPage) {
        return next(null, file)
      }

      if (sitemaps[file.src.component] == null) {
        sitemaps[file.src.component] = {
          filename: `sitemap-${file.src.component}.xml`,
          urls: [],
        }
      }

      sitemaps[file.src.component].urls.push({
        url: file.pub.absoluteUrl,
        version: file.src.version,
        lastmodISO,
      })

      next(null, file)
    },
    function (callback) {

      Object.values(sitemaps).forEach(({ filename, urls }) => {
        urls.sort(sortSitemapUrls)
        const sitemap = sm.createSitemap({ urls })
        this.push(new File({
          path: filename,
          contents: new Buffer(sitemap.toString()),
        }))
      })

      const sitemapIndexUrls = Object.values(sitemaps)
        .map(({ filename }) => `${siteUrl}/${filename}`)

      const sitemapIndex = sm.buildSitemapIndex({ urls: sitemapIndexUrls })
      this.push(new File({
        path: 'sitemap.xml',
        contents: new Buffer(sitemapIndex.toString()),
      }))

      callback()
    })
}

function sortSitemapUrls(one, two) {
  if (one.version !== two.version) {
    return sortByVersion(one.version, two.version)
  }
  return one.url.localeCompare(two.url)
}
