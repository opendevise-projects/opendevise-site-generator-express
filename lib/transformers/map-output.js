'use strict'

const path = require('path')
const map = require('map-stream')

module.exports = function () {

  return map((file, next) => {
    if (file.out != null) {
      file.path = path.relative('/', file.out.path)
    }
    next(null, file)
  })
}
