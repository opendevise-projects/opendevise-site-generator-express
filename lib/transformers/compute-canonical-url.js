'use strict'

const through2 = require('through2')
const sortByVersion = require('../helpers/sort-by-version')

module.exports = function () {

  const canonicalIndex = {}

  return through2.obj(
    function (file, enc, next) {
      const id = [file.src.component, file.src.module, ...file.src.topicSegments, file.src.stem].join(':')
      if (canonicalIndex[id] == null) {
        canonicalIndex[id] = []
      }
      canonicalIndex[id].push(file)
      next()
    },
    function (callback) {
      Object.entries(canonicalIndex).forEach(([pageId, files]) => {
        files.sort((a, b) => sortByVersion(a.src.version, b.src.version))
        const canonicalUrl = files[0].pub.absoluteUrl
        const versions = files.map((file) => ({ string: file.src.version, url: file.pub.url }))
        files.forEach((file) => {
          file.pub.canonicalUrl = canonicalUrl
          file.rel.versions = versions
          this.push(file)
        })
      })
      callback()
    })
}
