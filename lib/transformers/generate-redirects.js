'use strict'

const _ = require('lodash')
const File = require('vinyl')
const through2 = require('through2')

const { getSourceFromXref, expandSource, getOutput } = require('../helpers/locations')

module.exports = function ({ siteRootComponent, htmlUrlExtensionStrategy }) {

  const redirects = []

  return through2.obj(
    function (file, enc, next) {

      if (!file.src || file.src.area !== 'content' || file.aspectDomainPage) {
        return next(null, file)
      }

      const pageAliasesString = _.get(file, 'attributes.page-aliases')
      const pageAliases = (pageAliasesString != null) ? pageAliasesString.split(',') : []

      pageAliases
        .map((alias) => {

          if (alias.startsWith('/')) {
            return alias
          }

          const xref = alias.trim().replace(/\.adoc$/, '')
          const aliasSource = getSourceFromXref(xref)
          expandSource(aliasSource, file.src)
          const output = getOutput(aliasSource, siteRootComponent, htmlUrlExtensionStrategy)
          return output.url
        })
        .forEach((aliasUrl) => {
          redirects.push(`location = ${aliasUrl} { return 301 ${file.pub.url}; }`)
        })

      next(null, file)
    },
    function (callback) {
      const cleanRedirects = _(redirects)
        .sort()
        .uniq()
        .value()
        .join('\n')
      this.push(new File({
        path: 'nginx-redirects.conf',
        contents: new Buffer(cleanRedirects),
      }))
      callback()
    })
}
