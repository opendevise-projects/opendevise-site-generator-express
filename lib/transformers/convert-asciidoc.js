'use strict'

const map = require('map-stream')

module.exports = function () {

  return map((file, next) => {

    const html = file.ast.convert()
    file.contents = new Buffer(html)
    file.extname = '.html'

    // We don't need the AST anymore so let's help the garbage collector
    delete file.ast

    next(null, file)
  })
}
