'use strict'

const fs = require('fs')
const path = require('path')
const convict = require('convict')
const yaml = require('js-yaml')

const conf = convict({
  playbook: {
    doc: 'Location of the playbook',
    format: String,
    default: 'site.yml',
    env: 'PLAYBOOK',
    arg: 'playbook',
  },
  quiet: {
    doc: 'Do not write any messages to stdout',
    format: Boolean,
    default: false,
    arg: 'quiet',
  },
  silent: {
    doc: 'Suppress all messages',
    format: Boolean,
    default: false,
    arg: 'silent',
  },
  content: {
    doc: 'The list of repositories + version patterns to build',
    format: Array,
    default: [],
    env: 'CONTENT',
  },
  site: {
    url: {
      doc: 'The base URL of the published site (optional). Should not include a trailing slash.',
      format: String,
      default: undefined,
      arg: 'site-url',
    },
    title: {
      doc: 'The title of the site (optional).',
      format: String,
      default: undefined,
      arg: 'site-title',
    },
    root: {
      doc: 'The name of the component to use as the root of the site (optional).',
      format: String,
      default: undefined,
    },
    aspect: {
      doc: 'The name of the aspect navigation to make available on every page in the site.',
      format: String,
      default: undefined,
    },
    swiftype_key: {
      doc: 'The key to activate the SwiftType widget.',
      format: String,
      default: undefined,
      arg: 'swiftype-key',
    },
  },
  theme: {
    repository: {
      doc: 'git repo pointing to the theme',
      format: String,
      default: null,
    },
    tag: {
      doc: 'the tag (i.e., version) of the theme bundle to use',
      format: String,
      default: undefined
    },
    archive: {
      doc: 'use a local theme archive',
      format: String,
      default: '',
      arg: 'theme-archive',
    },
  },
  skip_theme_cache: {
    doc: 'set to true if you want the site generator to use a remote bundle (and skip the local cache of bundles)',
    format: Boolean,
    default: false,
    arg: 'skip-theme-cache',
  },
  nav: {
    doc: 'The list of descriptors which define the aspect navigation domains.',
    format: Array,
    default: undefined,
  },
  html_url_extension_strategy: {
    doc: 'Controls how the URL extension for HTML pages is handled (default, drop, or indexify).',
    format: String,
    default: 'default',
    arg: 'html-url-extension-strategy',
  },
  aspect_page_url_strategy: {
    doc: 'Controls how links to pages in aspect domains are generated (path or query).',
    format: String,
    default: 'path',
    arg: 'aspect-page-url-strategy',
  },
  redirects: {
    doc: 'Generate nginx config file containing URL redirects for page aliases.',
    format: Boolean,
    default: false,
    arg: 'redirects',
  },
})

let playbook = conf.get('playbook')
if (!playbook.endsWith('.yml')) {
  playbook = playbook + '.yml'
}
conf.load(yaml.safeLoad(fs.readFileSync(playbook, 'utf8')))
conf.set('playbook', playbook)

module.exports = conf
