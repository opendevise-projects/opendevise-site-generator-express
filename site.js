'use strict'

const startedAt = new Date().getTime()
const PROGRAM_NAME = 'OpenDevise Site Generator Express'

process.on('unhandledRejection', (reason) => {
  console.error('An unexpected error occurred when generating the site:')
  console.error(`Unhandled promise rejection: ${reason.stack}`)
  process.exitCode = 1
});

const fs = require('fs')
const path = require('path')

const _ = require('lodash')
const del = require('del')
const map = require('map-stream')
const merge = require('merge-stream')
const numeral = require('numeral')
const streamToArray = require('stream-to-array')
const through2 = require('through2')
const vfs = require('vinyl-fs')

const config = require('./config/index')
const vgit = require('./lib/helpers/vinyl-git')
const vinylGithubRelease = require('./lib/helpers/vinyl-github-release')

try {
  config.validate({ allowed: 'strict' })
}
catch (error) {
  const configErrorMessages = error.message.split('\n')
  configErrorMessages.forEach((message) => {
    console.error('Bad config -', message)
  })

  // 9 - Invalid Argument
  // https://nodejs.org/api/process.html#process_exit_codes
  process.exit(9)
}

const attributes = require('./lib/helpers/asciidoc-attributes')
const includeLocator = require('./lib/helpers/include-locator')
const templateModel = require('./lib/helpers/template-model')
const applyTemplate = require('./lib/transformers/apply-template')
const assignFileLocations = require('./lib/transformers/assign-file-locations')
const convertAsciiDoc = require('./lib/transformers/convert-asciidoc')
const duplicateAspectDomainPages = require('./lib/transformers/duplicate-aspect-domain-pages')
//const generateFileReport = require('./lib/transformers/generate-file-report')
const generateSitemap = require('./lib/transformers/generate-sitemap')
const mapOutput = require('./lib/transformers/map-output')
const computeCanonicalUrl = require('./lib/transformers/compute-canonical-url')
const readAsciiDoc = require('./lib/transformers/read-asciidoc')
const readNavigation = require('./lib/transformers/read-navigation')
const generateRedirects = require('./lib/transformers/generate-redirects')

const contentRepositories = config.get('content')
const siteTitle = config.get('site.title')
const siteUrl = config.get('site.url') || `file://${path.resolve('build/site')}`
const siteRootComponent = config.get('site.root')
const siteAspectDomain = config.get('site.aspect')
const swiftypeKey = config.get('site.swiftype_key')
const htmlUrlExtensionStrategy = config.get('html_url_extension_strategy')
const aspectPageUrlStrategy = config.get('aspect_page_url_strategy')
const redirects = config.get('redirects')
const silent = config.get('silent')
const quiet = silent || config.get('quiet')
const inCI = process.env.CI || process.env.BUILD_NUMBER

if (!silent) {
  console.log(`${PROGRAM_NAME} is preparing your site using the following playbook: ${config.get('playbook')}`)
}

del.sync('build/site/*')

const sources = {
  aspectNavigationDescriptors: config.get('nav'),
  content: {
    navigationDescriptors: ['modules/*/nav.adoc'],
    pages: [
      'modules/*/content/**/*.adoc',
      '!modules/*/nav.adoc',
      '!modules/*/content/**/_*.adoc',
      '!modules/*/content/_fragments/**/*.adoc',
    ],
    samplesAndFragments: [
      'modules/*/samples/**/*.*',
      'modules/*/content/**/_*.adoc',
      'modules/*/content/_fragments/**/*.*',
      '!modules/*/content/**/_attributes.adoc',
    ],
    images: ['modules/*/assets/images/**/*.*'],
    attachments: ['modules/*/assets/attachments/**/*.*'],
  },
  theme: {
    helpers: ['helpers/*.js'],
    layouts: ['layouts/*.hbs'],
    partials: ['partials/*.hbs'],
    assets: [
      '{font,fonts}/**/*.{eot,svg,woff,woff2}',
      '{img,images}/**/*.*',
      '{js,scripts}/**/*.*',
      '{css,stylesheets}/*.css',
    ],
  },
}

const { navigationDescriptors, pages, samplesAndFragments, images, attachments } = vgit(sources.content, {
  repositories: contentRepositories,
  defaultBranchMatch: '{v*,master}',
  componentDescFile: 'component.yml',
  quiet
})

const { helpers, layouts, partials, assets } = vinylGithubRelease(sources.theme, {
  repository: config.get('theme.repository'),
  tag: config.get('theme.tag'),
  skipThemeCache: config.get('skip_theme_cache'),
  themeArchive: config.get('theme.archive'),
  quiet
})

const includes = samplesAndFragments
  .pipe(assignFileLocations({ siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy }))

const resolvedAspectNavigationDescriptors = []
const navigationIndexStream = merge(_.compact([
    navigationDescriptors
      .pipe(assignFileLocations({ type: 'linked-navigation', siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy })),
    (sources.aspectNavigationDescriptors || []).length > 0 ?
      vfs.src(sources.aspectNavigationDescriptors, { base: process.cwd(), cwd: process.cwd() })
        .pipe(map((file, next) => {
          resolvedAspectNavigationDescriptors.push(file.relative)
          next(null, file)
        }))
        .pipe(assignFileLocations({ type: 'aspect-navigation', siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy })) :
      undefined
  ]))
  .pipe(readAsciiDoc({
    silent,
    siteRootComponent,
    includes,
    includeLocator,
    htmlUrlExtensionStrategy,
    aspectPageUrlStrategy,
  }))
  .pipe(readNavigation({ nav: resolvedAspectNavigationDescriptors, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy }))

const navigation = streamToArray(navigationIndexStream)
  .then(([file]) => file.navigation)

merge([
  navigationIndexStream,

  pages
    .pipe(assignFileLocations({ siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy }))
    .pipe(readAsciiDoc({
      silent,
      siteRootComponent,
      attributes,
      includes,
      includeLocator,
      htmlUrlExtensionStrategy,
    }))
    .pipe(convertAsciiDoc())
    .pipe(computeCanonicalUrl())
    .pipe(duplicateAspectDomainPages(navigation, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy))
    .pipe(applyTemplate({
      helpers,
      layouts,
      partials,
      templateModel: templateModel(navigation, siteTitle, siteUrl, siteRootComponent, siteAspectDomain, swiftypeKey),
    }))
    //.pipe(generateFileReport())
    .pipe(generateSitemap({ siteUrl }))
    .pipe(redirects ? generateRedirects({ siteRootComponent, htmlUrlExtensionStrategy }) : through2.obj()),

  assets
    .pipe(assignFileLocations({ type: 'theme', siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy })),

  merge([images, attachments])
    .pipe(assignFileLocations({ siteUrl, siteRootComponent, htmlUrlExtensionStrategy, aspectPageUrlStrategy })),
])
  .pipe(mapOutput())
  .pipe(vfs.dest('build/site'))
  .on('error', (e) => {
    console.error('An unexpected error occurred when generating the site:')
    console.error('error', e)
    process.exitCode = 1
  })
  .on('end', () => {
    if (!silent) {
      const elapsed = (new Date().getTime() - startedAt) / 1000
      console.log(`${PROGRAM_NAME} completed successfully in: ${numeral(elapsed).format('00:00:00')}`)
      console.log('Your new site can be viewed at ' + (inCI ? siteUrl : `file://${path.resolve('build/site')}`))
    }
  })
